package de.qlearning.iv.model;

import com.google.gson.annotations.SerializedName;


public class Course {

    @SerializedName("id")
    private int id = -1;

    @SerializedName("title")
    private String title = "";

    @SerializedName("university_id")
    private int university_id;

    @SerializedName("has_training")
    private boolean has_training;

    @SerializedName("has_exam")
    private boolean has_exam;

    @SerializedName("has_flashcards")
    private boolean has_flashcards;

    @SerializedName("partner_company_ids")
    private int[] partner_companies = new int[0];

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public int getUniversityId() {
        return university_id;
    }
    public void setUniversityId(int id) {
        university_id = id;
    }

    public void setHasFlashCards(boolean b) { has_flashcards = b;}
    public boolean hasFlashCards() { return has_flashcards; }

    public void setHasTrainingMode(boolean b) { has_training = b;}
    public boolean hasTraining() { return has_training; }

    public void setHasExamMode(boolean b) { has_exam = b;}
    public boolean hasExam() { return has_exam; }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        if (id != course.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return title;
    }

}
